import { BrowserRouter } from "react-router-dom";
import AppToolbar from "./header";
import AppRoutes from "./routes";

function App() {
    return(
       <BrowserRouter>
            <AppToolbar />
                <AppRoutes />
       </BrowserRouter> 
    )
}
export default App;