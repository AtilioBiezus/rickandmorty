import { Route, Switch, Redirect } from "react-router";
import CharacterInfo from "../pages/characterInfo";
import Characters from "../pages/characters";
import Home from "../pages/home";
import Locations from "../pages/locations";
import LocationInfo from "../pages/locationInfo";

function AppRoutes() {
    return(
        <Switch>
            <Route path="/characters" component={Characters}/>
            <Route path="/locations" component={Locations}/>
            <Route path="/character/:id" component={CharacterInfo}/>
            <Route path="/location/:id" component={LocationInfo}/>
            <Route path="/home" component={Home}/>
            <Route path="/" component={() => <Redirect to="/home"/>}/>
        </Switch>
    )
}

export default AppRoutes;