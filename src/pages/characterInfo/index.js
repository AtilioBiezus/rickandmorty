import { Container, Paper, Typography } from "@material-ui/core";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";
import { useParams } from "react-router";

function CharacterInfo() {

    const [ charInfo, setCharInfo ] = useState();

    const { id } = useParams();

    useEffect(() => {
        getCharInfo();
    }, [])

    function getCharInfo() {
        axios.get(`https://rickandmortyapi.com/api/character/${id}`)
            .then((info) => {
                setCharInfo(info.data)
            })
    }

    return (
        <Container>
            <Paper>
                <Container align='center'>
                    <img src={charInfo?.image} style={{ objectFit: 'cover', height: "200px" }} />
                </Container>
                <Typography fontWeight="bold" variant="subtitle1" align='center'>
                    Name - {charInfo?.name}
                </Typography>
                <Typography fontWeight="bold" variant="subtitle1" align='center'>
                    Last Location - {charInfo?.location.name}
                </Typography>
                <Typography fontWeight="bold" variant="subtitle1" align='center'>
                    Specie - {charInfo?.species}
                </Typography>
                <Typography fontWeight="bold" variant="subtitle1" align='center'>
                    Status - {charInfo?.status}
                </Typography>
            </Paper>
        </Container>
    )
}

export default CharacterInfo;