import { Container, Paper, Typography } from "@material-ui/core";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";
import { useParams } from "react-router";

function LocationInfo() {

    const [ charInfo, setCharInfo ] = useState();

    const { id } = useParams();

    useEffect(() => {
        getCharInfo();
    }, [])

    function getCharInfo() {
        axios.get(`https://rickandmortyapi.com/api/location/${id}`)
            .then((info) => {
                setCharInfo(info.data)
            })
    }

    return (
        <Container>
            <Paper>
                <Typography fontWeight="bold" variant="subtitle1" align='center'>
                    Name - {charInfo?.name}
                </Typography>
                <Typography fontWeight="bold" variant="subtitle1" align='center'>
                    Dimension - {charInfo?.dimension}
                </Typography>
                <Typography fontWeight="bold" variant="subtitle1" align='center'>
                    Created in - {charInfo?.created}
                </Typography>
            </Paper>
        </Container>
    )
}

export default LocationInfo;