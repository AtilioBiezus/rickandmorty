import { Button, Container, Link } from "@material-ui/core";
import { Stack } from "@mui/material";

function Home() {
    return(
        <Container align="center">
            <img src="/rickandmorty.jpg" height="600px"/>
            <Stack spacing={2} width='20%'>
                <Button size='large'><Link href="/characters" underline="none">Characters</Link></Button>
                <Button size='large'><Link href="/locations" underline="none">Locations</Link></Button>
            </Stack>
        </Container>
    )
}

export default Home;