import { useState, useEffect } from "react";
import { Container, Grid, Paper, Typography } from "@material-ui/core";
import axios from "axios";
import { useHistory } from "react-router";

function Locations() {

    const [ char, setChar ] = useState([]);
    const history = useHistory(); 

    useEffect(() => {
        getLocations();   
    }, [])

    function getLocations() {
        axios.get("https://rickandmortyapi.com/api/location")
            .then((locations) => {
                setChar(locations.data.results)
            });
    }

    return(
        <Grid container spacing={2}>
            {char.map((chars) => (
                <Grid item md={3} sm={6} xs={12} padding={2} onClick={() => history.push(`/location/${chars.id}`)}>
                    <Paper>
                                                
                        <Typography fontWeight="bold" variant="subtitle1" align='center'>
                             Name - {chars.name}
                        </Typography>
                        <Typography fontWeight="bold" variant="subtitle1" align='center'>
                            Type - {chars.type}
                        </Typography>
                    </Paper>
                </Grid>
            ))}
        </Grid>
    )
}

export default Locations;