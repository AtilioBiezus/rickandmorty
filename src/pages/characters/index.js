import { useState, useEffect } from "react";
import { Container, Grid, Paper, Typography } from "@material-ui/core";
import axios from "axios";
import { useHistory } from "react-router";

function Characters() {

    const [ char, setChar ] = useState([]);
    const history = useHistory();

    useEffect(() => {
        getCharacters();   
    }, [])

    function getCharacters() {
        axios.get("https://rickandmortyapi.com/api/character")
            .then((characters) => {
                setChar(characters.data.results)
            });
    }

    return(
        <Grid container spacing={2}>
            {char.map((chars) => (
                <Grid item md={3} sm={6} xs={12} padding={2} onClick={() => history.push(`/character/${chars.id}`)}>
                    <Paper>
                        <Container align='center'>
                            <img src={chars.image} style={{ objectFit: 'cover', height: "200px" }} />
                        </Container>
                        
                        <Typography fontWeight="bold" variant="subtitle1" align='center'>
                            {chars.name}
                        </Typography>
                    </Paper>
                </Grid>
            ))}
        </Grid>
    )
}

export default Characters;