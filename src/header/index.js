import { AppBar, Button, Toolbar, Typography } from "@material-ui/core";
import { useHistory } from "react-router-dom";

const AppToolbar = () => {

    const history = useHistory();

    return (
        <AppBar position="sticky" color='secondary'>
            <Toolbar>
                <Button color="inherit">
                    <Typography variant="h6" sx={{flexGrow: 1 }} onClick={() => history.push("/home")}>Rick and Morty</Typography>
                </Button>
            </Toolbar>
        </AppBar>
    )
}

export default AppToolbar;